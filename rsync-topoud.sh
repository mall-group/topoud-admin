#! /bin/bash
echo "0cb4cda33e"
rsync -ar -v \
  --delete \
  --exclude "node_modules/" \
  --exclude ".git/" \
  --exclude ".DS_Store" \
  --exclude ".idea" \
  --exclude "npm-debug.log" \
  ../topoud-admin \
  root@221.229.196.191:/home/wwwroot
var path = require('path');
var express = require('express');
var app = express();
var proxy = require('http-proxy-middleware');

var PORT = process.env.PORT || 9999;

app.use(express.static(path.resolve(__dirname, '../dist')));

app.use('/api/*', proxy({
    target: 'http://localhost:8081',
    // target: 'http://mall.jaxin.net',
    changeOrigin: true,
    // headers: {
    //     Host: 'xmall.exrick.cn',
    //     Referer: 'http://xmall.exrick.cn'
    // }
}));

app.get('*', function (req, res, next) {
    res.sendFile(path.join(__dirname, '../dist/index.html'))
});

app.listen(PORT, '0.0.0.0', function (err) {
    if (err) {
        return console.error(err);
    }

    console.log(`listening on ${PORT}...`)
});

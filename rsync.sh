#! /bin/bash

rsync -ar -v \
  --delete \
  --exclude "node_modules/" \
  --exclude ".git/" \
  --exclude ".DS_Store" \
  --exclude ".idea" \
  --exclude "npm-debug.log" \
  --exclude "psd/" \
  --exclude "/web/tinymce4.7.5/" \
  ../xmall-front \
  root@139.196.109.224:/home/wwwroot

rsync -ar -v \
  --delete \
  --exclude "node_modules/" \
  --exclude ".git/" \
  --exclude ".DS_Store" \
  --exclude ".idea" \
  --exclude "npm-debug.log" \
  --exclude "psd/" \
  --exclude "/web/tinymce4.7.5/" \
  ../xmall-front \
  root@139.196.192.219:/home/wwwroot

  rsync -ar -v \
  --delete \
  --exclude "node_modules/" \
  --exclude ".git/" \
  --exclude ".DS_Store" \
  --exclude ".idea" \
  --exclude "npm-debug.log" \
  --exclude "psd/" \
  --exclude "/web/tinymce4.7.5/" \
  ../xmall-front \
  root@106.14.197.243:/home/wwwroot

import http from './public'
//
export const getCommissionList = (params) => {
  return http.fetchGet('/api/commission/getCommissionList', params)
}
export const getCommission = (params) => {
  return http.fetchGet('/api/commission/getCommission', params)
}
export const saveCommission = (params) => {
  return http.fetchPost('/api/commission/save', params)
}
export const deleteCommission = (params) => {
  return http.fetchPost('/api/commission/delete', params)
}
export const updateCommissionStatus = (params) => {
  return http.fetchPost('/api/commission/updateStatus', params)
}

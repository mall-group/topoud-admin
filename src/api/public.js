import axios from 'axios'
import { getStore } from '../utils/storage'

axios.defaults.timeout = 500000
axios.defaults.headers.post['Content-Type'] = 'application/x-www=form-urlencoded'
const API_HOST = ''

function getHeader () {
  return {
    Authorization: `Bearer ${getStore('token')}`
  }
}

export default {
  fetchGet (url, params = {}) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'GET',
        url: API_HOST + url,
        params: params.params,
        headers: getHeader()
      }).then(res => {
        resolve(res.data)
      }).catch(error => {
        reject(error)
      })
    })
  },
  fetchQuickSearch (url) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'GET',
        url: API_HOST + url,
        headers: getHeader()
      }).then(res => {
        resolve(res.data)
      }).catch(error => {
        reject(error)
      })
    })
  },
  fetchPost (url, params = {}) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'POST',
        url: API_HOST + url,
        data: params,
        headers: getHeader()
      }).then(res => {
        resolve(res.data)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

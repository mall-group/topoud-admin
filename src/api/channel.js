import http from './public'

// 分页获取组织列表
export const getGroupListByPage = (params) => {
  return http.fetchGet('/api/channel/getGroupListByPage', params)
}
// 获取组织列表
export const getGroupList = (params) => {
  return http.fetchGet('/api/channel/getGroupList', params)
}
// 分佣列表查询
export const getChannelLogListByPage = (params) => {
  return http.fetchGet('/api/channel/getChannelLogListByPage', params)
}
// 获取组织信息
export const getGroupInfo = (params) => {
  return http.fetchGet('/api/channel/getGroupInfoById', params)
}
// 保存组织
export const saveGroup = (params) => {
  return http.fetchPost('/api/channel/saveGroup', params)
}
// 删除组织
export const deleteGroup = (params) => {
  return http.fetchPost('/api/channel/deleteGroup', params)
}
// 更新状态
export const updateStatus = (params) => {
  return http.fetchPost('/api/channel/updateStatus', params)
}
// 获取组织成员
export const getMemberList = (params) => {
  return http.fetchGet('/api/channel/getMemberList', params)
}
// 加入组织
export const addMember = (params) => {
  return http.fetchPost('/api/channel/addMember', params)
}
// 删除组织成员
export const removeMember = (params) => {
  return http.fetchPost('/api/channel/removeMember', params)
}

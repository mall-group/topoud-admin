import http from './public'
// 登陆
export const userLogin = (params) => {
  return http.fetchPost('/api/member/login', params)
}
// 退出登陆
export const loginOut = (params) => {
  return http.fetchGet('/api/member/loginOut', params)
}
// 注册账号
export const register = (params) => {
  return http.fetchPost('/api/member/register', params)
}
// 找回密码
export const findpwd = (params) => {
  return http.fetchPost('/api/member/findPwd', params)
}
// 重置密码
export const resetPassword = (params) => {
  return http.fetchPost('/api/member/resetPassword', params)
}
// 上传图片
export const upload = (params) => {
  return http.fetchPost('/api/common/uploadCompressImage', params)
}
// 修改头像
export const updateAvatar = (params) => {
  return http.fetchPost('/api/member/updateAvatar', params)
}
// 捐赠列表
export const thanksList = (params) => {
  return http.fetchGet('/api/member/thanks', params)
}
// 首页接口
export const productHome = (params) => {
  return http.fetchGet('/api/store/home', params)
}
// 推荐板块
export const recommend = (params) => {
  return http.fetchGet('/api/goods/recommend', params)
}
// 捐赠板块
export const thank = (params) => {
  return http.fetchGet('/api/goods/thank', params)
}
// 极验验证码
export const geetest = (params) => {
  return http.fetchGet('/api/member/geetestInit?t=' + (new Date()).getTime(), params)
}
// 更新密码
export const updatePassword = (params) => {
  return http.fetchPost('/api/member/updatePassword', params)
}
// 更新邮箱
export const updateEmail = (params) => {
  return http.fetchPost('/api/member/updateEmail', params)
}


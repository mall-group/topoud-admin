import http from './public'
// 保存模板
export const saveTemplate = (params) => {
  return http.fetchPost('/api/template/saveTemplate', params)
}
// 删除模板
export const deleteTemplate = (params) => {
  return http.fetchPost('/api/template/deleteTemplate', params)
}
// 选择使用模板
export const selectAndUseTemplate = (params) => {
  return http.fetchPost('/api/template/selectAndUseTemplate', params)
}
// 获取首页板块列表
export const getTemplateListByPage = (params) => {
  return http.fetchGet('/api/template/getTemplateListByPage', params)
}
// 获取模板内容
export const getTemplateInfo = (params) => {
  return http.fetchGet('/api/template/getTemplateInfo', params)
}
// 行业
export const getIndustryTree = (params) => {
  return http.fetchGet('/api/icard/getIndustryTree', params)
}
// 场景
export const getConditions = () => {
  return http.fetchGet('/javaapi/card/template/queryCardTempllateCondition')
}

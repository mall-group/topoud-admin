import http from './public'

// 获取品牌列表
export const getBrandList = (params) => {
  return http.fetchGet('/api/brand/getBrandListByPage', params)
}
// 获取所有品牌
export const getAllBrand = () => {
  return http.fetchGet('/api/brand/getBrandListByStoreId')
}
// 获取品牌信息
export const getBrandInfoById = (params) => {
  return http.fetchGet('/api/brand/getBrandInfoById', params)
}
// 保存品牌信息
export const saveBrandInfo = (params) => {
  return http.fetchPost('/api/brand/brandSave', params)
}
// 删除品牌信息
export const delBrand = (params) => {
  return http.fetchPost('/api/brand/deleteBrand', params)
}

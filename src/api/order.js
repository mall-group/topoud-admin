import http from './public'
// 生成订单
export const submitOrder = (params) => {
  return http.fetchPost('/api/order/addOrder', params)
}
// 确认支付
export const payMent = (params) => {
  return http.fetchPost('/api/order/payOrder', params)
}
// 支付二维码
export const nativePay = (params) => {
  return http.fetchPost('/api/wechat/nativePay', params)
}
// 获取用户订单
export const orderList = (params) => {
  return http.fetchGet('/api/order/orderList', params)
}
// 获取单个订单详情
export const getOrderDet = (params) => {
  return http.fetchGet('/api/order/orderDetail', params)
}
// 取消订单
export const cancelOrder = (params) => {
  return http.fetchPost('/api/order/cancelOrder', params)
}
// 删除订单
export const delOrder = (params) => {
  return http.fetchGet('/api/order/delOrder', params)
}
// 发货
export const submitDelivery = (params) => {
  return http.fetchPost('/api/order/orderShipping', params)
}
// 获取订单列表
export const getOrderList = (params) => {
  return http.fetchGet('/api/order/getOrderList', params)
}
// 退货审核
export const returnConfirm = (params) => {
  return http.fetchPost('/api/order/returnConfirm', params)
}
// 退款
export const returnPay = (params) => {
  return http.fetchPost('/api/order/returnPay', params)
}
// 退款
export const getReturnList = (params) => {
  return http.fetchGet('/api/order/getReturnList', params)
}


import http from './public'
// 活动记录列表
export const getActivityLogList = (params) => {
  return http.fetchGet('/api/activityLog/getActivityLogList', params)
}
export const getActivityLog = (params) => {
  return http.fetchGet('/api/activityLog/getActivityLog', params)
}

import http from './public'

// 分页获取操作员列表
export const getOperatorListByPage = (params) => {
  return http.fetchGet('/api/operator/getOperatorListByPage', params)
}
// 获取操作员列表
export const getOperatorList = (params) => {
  return http.fetchGet('/api/operator/getOperatorList', params)
}
// 操作员信息
export const getOperatorInfo = (params) => {
  return http.fetchGet('/api/operator/getOperatorInfo', params)
}
// 保存操作员
export const save = (params) => {
  return http.fetchPost('/api/operator/save', params)
}
// 删除操作员
export const deleteMember = (params) => {
  return http.fetchPost('/api/operator/deleteMember', params)
}

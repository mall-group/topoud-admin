import http from './public'
// 保存首页板块
export const savePanel = (params) => {
  return http.fetchPost('/api/storeWeb/savePanel', params)
}
// 删除首页板块
export const deletePanel = (params) => {
  return http.fetchPost('/api/storeWeb/deletePanel', params)
}
// 获取首页板块列表
export const getPanelList = (params) => {
  return http.fetchGet('/api/storeWeb/getPanelList', params)
}
// 保存首页板块内容
export const savePanelContent = (params) => {
  return http.fetchPost('/api/storeWeb/savePanelContent', params)
}
// 删除首页板块内容
export const deletePanelContent = (params) => {
  return http.fetchGet('/api/storeWeb/deletePanelContent?panelContentId=' + params)
}
// 获取首页板块内容列表
export const getPanelContentList = (params) => {
  return http.fetchGet('/api/storeWeb/getPanelContentList?panelId=' + params)
}
// 获取首页板块内容
export const getPanelContent = (params) => {
  return http.fetchGet('/api/storeWeb/getPanelContent?panelContentId=' + params)
}

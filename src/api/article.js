import http from './public'

// 获取品牌列表
export const getArticleList = (params) => {
  return http.fetchGet('/api/article/getArticleListByPage', params)
}
// 获取所有品牌
export const getAllBrand = () => {
  return http.fetchGet('/api/brand/getBrandListByStoreId')
}
// 获取文章信息
export const getArticleInfo = (params) => {
  return http.fetchGet('/api/article/getArticleInfo', params)
}
// 保存文章
export const saveArticle = (params) => {
  return http.fetchPost('/api/article/saveArticle', params)
}
// 保存文章主题
export const saveArticleTheme = (params) => {
  return http.fetchPost('/api/article/saveArticleTheme', params)
}
// 删除文章主题
export const deleteArticleTheme = (params) => {
  return http.fetchPost('/api/article/deleteArticleTheme', params)
}
// 删除文章
export const deleteArticle = (params) => {
  return http.fetchPost('/api/article/deleteArticle', params)
}
// 更新状态
export const updateArticleStatus = (params) => {
  return http.fetchPost('/api/article/updateStatus', params)
}
// 获取文章分类列表
export const getArticleTypeList = (params) => {
  return http.fetchGet('/api/article/getArticleTypeList', params)
}
// 获取文章分类信息
export const getArticleTypeInfo = (params) => {
  return http.fetchGet('/api/article/getArticleTypeInfo', params)
}
// 保存文章分类
export const saveArticleType = (params) => {
  return http.fetchPost('/api/article/saveArticleType', params)
}
// 删除文章
export const deleteArticleType = (params) => {
  return http.fetchPost('/api/article/deleteArticleType', params)
}
// 更新状态
export const updateArticleTypeStatus = (params) => {
  return http.fetchPost('/api/article/updateTypeStatus', params)
}

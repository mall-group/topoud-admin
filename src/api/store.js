import http from './public'
// 保存店铺数据
export const saveStore = (params) => {
  return http.fetchPost('/api/store/saveStore', params)
}
// 店铺信息
export const getStoreInfo = (params) => {
  return http.fetchGet('/api/store/getStoreInfo', params)
}
// 保存首页板块
export const savePanel = (params) => {
  return http.fetchPost('/api/template/savePanel', params)
}
// 删除首页板块
export const deletePanel = (params) => {
  return http.fetchPost('/api/template/deletePanel', params)
}
// 获取首页板块列表
export const getPanelList = (params) => {
  return http.fetchGet('/api/template/getPanelList', params)
}
// 保存首页板块内容
export const savePanelContent = (params) => {
  return http.fetchPost('/api/template/savePanelContent', params)
}
// 删除首页板块内容
export const deletePanelContent = (params) => {
  return http.fetchGet('/api/template/deletePanelContent?id=' + params)
}
// 获取首页板块内容列表
export const getPanelContentList = (params) => {
  return http.fetchGet('/api/template/getPanelContentList', params)
}
// 获取首页板块内容
export const getPanelContent = (params) => {
  return http.fetchGet('/api/template/getPanelContent?panelContentId=' + params)
}
// 获取活动列表
export const getActivityType = () => {
  return http.fetchGet('/api/activity/getActivityType')
}
// 店铺首页
export const getShopIndex = () => {
  return http.fetchGet('/api/store/dashboard')
}
// 获取店铺名称，无需认证
export const getStoreNameById = (params) => {
  return http.fetchGet('/api/store/getStoreName', params)
}
// 设置-保存海报设置
export const savePoster = (params) => {
  return http.fetchPost('/api/store/savePoster', params)
}
// 全局参数设置
export const saveConfig = (params) => {
  return http.fetchPost('/api/store/saveConfig', params)
}
// 获取全局参数
export const getConfig = (params) => {
  return http.fetchGet('/api/store/getConfig', params)
}

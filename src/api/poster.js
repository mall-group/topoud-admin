import http from './public'

// 分页获取海报列表
export const getPosterTemplateListByPage = (params) => {
  return http.fetchGet('/api/poster/getPosterTemplateListByPage', params)
}
// 海报信息
export const getPosterTemplate = (params) => {
  return http.fetchGet('/api/poster/getPosterTemplate', params)
}
// 保存、更新
export const savePosterTemplate = (params) => {
  return http.fetchPost('/api/poster/savePosterTemplate', params)
}
// 更新状态
export const updatePosterTemplateStatus = (params) => {
  return http.fetchPost('/api/poster/updatePosterTemplateStatus', params)
}
// 更新默认
export const setDefaultPosterTemplate = (params) => {
  return http.fetchPost('/api/poster/setDefaultPosterTemplate', params)
}
// 删除
export const deletePosterTemplate = (params) => {
  return http.fetchPost('/api/poster/deletePosterTemplate', params)
}

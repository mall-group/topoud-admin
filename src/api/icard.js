import http from './public'

// 获取品牌列表
export const getMemberLevel = (params) => {
  return http.fetchGet('/api/icard/member/getMemberLevel', params)
}

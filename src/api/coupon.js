import http from './public'
// 店主本人
// 获取优惠券模式
export const getCouponModel = (params) => {
  return http.fetchGet('/api/coupon/getCouponModel', params)
}
// 添加优惠券
export const add = (params) => {
  return http.fetchPost('/api/coupon/addCoupon', params)
}
// 添加优惠券
export const start = (params) => {
  return http.fetchPost('/api/coupon/startCoupon', params)
}
// 添加优惠券
export const stop = (params) => {
  return http.fetchPost('/api/coupon/stopCoupon', params)
}
// 添加优惠券
export const del = (params) => {
  return http.fetchPost('/api/coupon/delCoupon', params)
}
// 赠送优惠券
export const give = (params) => {
  return http.fetchPost('/api/coupon/giveCoupon', params)
}
// 优惠券列表
export const getCouponList = (params) => {
  return http.fetchGet('/api/coupon/couponList', params)
}

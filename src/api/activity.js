import http from './public'
// 活动商品列表
export const getActivityList = (params) => {
  return http.fetchGet('/api/activity/getActivityList', params)
}
// 获取活动商品
export const getActivity = (params) => {
  return http.fetchGet('/api/activity/getActivity', params)
}
// 保存分享功能
export const saveActivity = (params) => {
  return http.fetchPost('/api/activity/saveActivity', params)
}
// 删除活动
export const delActivity = (params) => {
  return http.fetchPost('/api/activity/delActivity', params)
}
// 更新活动状态
export const updateActivity = (params) => {
  return http.fetchPost('/api/activity/updateActivity', params)
}

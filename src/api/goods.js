import http from './public'
// 商品类目(所有)
export const getCategory = (params) => {
  return http.fetchGet('/api/category/getCategoryTree', params)
}
// 商城商品类目(异步加载子类目)
export const getCategoryByparentId = (params) => {
  return http.fetchGet('/api/category/getCategoryListByParentId', params)
}
// 新增/编辑类目
export const saveCategory = (params) => {
  return http.fetchPost('/api/category/saveCategory', params)
}
// 删除类目
export const delCategory = (params) => {
  return http.fetchPost('/api/category/deleteCategory', params)
}
// 导入类目
export const loadCategoryByType = (params) => {
  return http.fetchGet('/api/category/loadCategoryByType', params)
}
// 一键清空类目
export const clean = () => {
  return http.fetchGet('/api/category/clean')
}
// 商品列表
export const getAllGoods = (params) => {
  return http.fetchGet('/api/goods/allGoods', params)
}
// 根据商家编号获取商品列表
export const getGoodsByStoreId = (params) => {
  return http.fetchGet('/api/goods/getGoodsByStoreId', params)
}
// 根据商家编号获取商品详细属性
export const selectGoods = (params) => {
  return http.fetchGet('/api/goods/selectGoods', params)
}
// 获取用户地址
export const addressList = (params) => {
  return http.fetchPost('/api/address/addressList', params)
}
// 通过id获取地址
export const getAddress = (params) => {
  return http.fetchPost('/api/address/address', params)
}
// 修改收货地址
export const addressUpdate = (params) => {
  return http.fetchPost('/api/address/updateAddress', params)
}
// 添加收货地址
export const addressAdd = (params) => {
  return http.fetchPost('/api/address/addAddress', params)
}
// 删除收货地址
export const addressDel = (params) => {
  return http.fetchPost('/api/address/delAddress', params)
}
// 商品详情
export const productDet = (params) => {
  return http.fetchGet('/api/goods/productDet?productId=' + params)
}
// 商品列表
export const getSearch = (params) => {
  return http.fetchGet('/api/goods/search', params)
}
// 快速搜索
export const getQuickSearch = (key) => {
  return http.fetchQuickSearch(`http://127.0.0.1:9200/item/itemList/_search?q=productName: ${key}`)
}
// 保存商品
export const saveGoods = (params) => {
  return http.fetchPost('/api/goods/saveGoods', params)
}
// 删除商品
export const delGoods = (params) => {
  return http.fetchGet('/api/goods/delGoods', params)
}
// 可代理商品
export const agentGoods = (params) => {
  return http.fetchGet('/api/goods/allAgentGoods', params)
}
// 更新商品状态
export const updateGoodsStatus = (params) => {
  return http.fetchPost('/api/goods/updateGoodsStatus', params)
}
// 获取规格信息
export const getSkuList = (params) => {
  return http.fetchGet('/api/sku/getSkuList', params)
}
// 保存规格信息
export const saveSku = (params) => {
  return http.fetchPost('/api/sku/saveTag', params)
}

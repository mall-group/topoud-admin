import http from './public'

// 获取操作员列表
export const getNotifyConfig = (params) => {
  return http.fetchGet('/api/notify/getNotifyConfig', params)
}
// 保存操作员
export const saveNotifyConfig = (params) => {
  return http.fetchPost('/api/notify/saveNotifyConfig', params)
}
// 删除操作员
export const deleteMember = (params) => {
  return http.fetchPost('/api/operator/deleteMember', params)
}

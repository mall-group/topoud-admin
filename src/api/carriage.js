import http from './public'
// 获取运费模板
export const getCarriageList = (params) => {
  return http.fetchGet('/api/carriage/getCarriageList', params)
}

// 保存运费模板
export const saveCarriage = (params) => {
  return http.fetchPost('/api/carriage/saveCarriage', params)
}

// 删除运费模板
export const deleteCarriage = (params) => {
  return http.fetchPost('/api/carriage/deleteCarriage', params)
}

// 查询运费模板详情
export const getCarriage = (params) => {
  return http.fetchGet('/api/carriage/getCarriage', params)
}

import http from './public'
// 满即送-保存
export const saveManjisong = (params) => {
  return http.fetchPost('/api/promotion/manjiusong_save', params)
}
// 满即送-获取数据
export const getManjisongInfo = (params) => {
  return http.fetchGet('/api/promotion/manjiusong_getInfo', params)
}

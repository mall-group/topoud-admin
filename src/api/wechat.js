import http from './public'
// 保存公众号数据
export const saveWechat = (params) => {
  return http.fetchPost('/api/wechat/saveWechat', params)
}
// 删除公众号（软删除）
export const delWechat = (params) => {
  return http.fetchPost('/api/wechat/delWechat', params)
}
// 公众号信息
export const getWechatInfo = (params) => {
  return http.fetchGet('/api/wechat/getWechatInfo', params)
}
// 获取公众号列表
export const getWechatList = (params) => {
  return http.fetchGet('/api/wechat/getWechatList', params)
}
// 根据ID获取公众号接口相关参数，url/token
export const getInterfaceParams = (params) => {
  return http.fetchGet('/api/wechat/getInterfaceParams', params)
}
// 保存菜单数据
export const saveWechatMenu = (params) => {
  return http.fetchPost('/api/wechat/saveWechatMenu', params)
}
// 获取菜单数据
export const getWechatMenu = (params) => {
  return http.fetchGet('/api/wechat/getWechatMenu', params)
}
// 保存自定义回复数据
export const saveWechatReply = (params) => {
  return http.fetchPost('/api/wechat/saveWechatReply', params)
}
// 获取自定义回复列表
export const getWechatReplyList = (params) => {
  return http.fetchGet('/api/wechat/getWechatReplyList', params)
}
// 获取自定义回复数据
export const getWechatReplyInfo = (params) => {
  return http.fetchGet('/api/wechat/getWechatReplyInfo', params)
}
// 获取粉丝列表
export const getFansList = (params) => {
  return http.fetchGet('/api/wechat/getFansList', params)
}
// 微信连接验证
export const test = (params) => {
  return http.fetchGet('/api/wechat/test', params)
}
// 素材列表
export const getMaterialList = (params) => {
  return http.fetchGet('/api/wechat/getMaterialList', params)
}
// 保存素材
export const saveMaterial = (params) => {
  return http.fetchPost('/api/wechat/saveMaterial', params)
}
// 根据ID获取素材素材
export const getMaterialByID = (params) => {
  return http.fetchGet('/api/wechat/getMaterialByID', params)
}
// 根据ID删除素材
export const deleteMaterialById = (params) => {
  return http.fetchGet('/api/wechat/getMaterialByID', params)
}
// 微信支付参数设置
export const payParamsSetting = (params) => {
  return http.fetchPost('/api/wechat/payParamsSetting', params)
}

module.exports = {
  getCrumbList: function (param) {
    let terminal = param.terminal
    let page = param.page
    let crumbList = []
    let indexItem = (terminal === '1') ? {name: '移动端首页设置'} : {name: 'PC端首页设置'}
    crumbList.push(indexItem)
    let arr = []
    switch (page) {
      case 'storePanel':
        arr = [
          {name: '首页板块设置'}
        ]
        break
      case 'storePanelEdit':
        arr = [
          {name: '首页板块设置', href: '/store/storePanel?terminal=' + terminal},
          {name: '创建板块'}
        ]
        break
      case 'storePanelContent':
        arr = [
          { name: `板块内容设置` }
        ]
        break
      case 'storePanelContentEdit':
        arr = [
          {name: '板块内容设置', href: '/store/storePanelContent?terminal=' + terminal},
          {name: `'${param.panelName || ''}'展示内容设置`}
        ]
        break
      case 'storeCarousel':
        arr = [
          {name: '轮播图设置'}
        ]
        break
      case 'storeCarouselEdit':
        arr = [
          {name: '轮播图设置', href: '/store/storeCarousel?terminal=' + terminal},
          {name: '轮播图内容设置'}
        ]
        break
    }
    crumbList = crumbList.concat(arr)
    return crumbList
  }
}

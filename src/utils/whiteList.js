export default [
  '/home',
  '/goods',
  '/login',
  '/register',
  '/findpwd',
  '/resetpwd',
  '/goodsDetails',
  '/thanks',
  '/search',
  '/refreshsearch',
  '/refreshgoods'
]

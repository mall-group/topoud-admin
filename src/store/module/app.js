import { isEmpty } from 'lodash'
import http from '../../api/public'

const app = {
  state: {
    userInfo: {},
    isLogined: false,
    userLoading: true
  },
  mutations: {
    getUserInfo: function (state, info) {
      state.userInfo = info
      state.isLogined = !isEmpty(info)
    },
    setUserLoading: function (state, loading) {
      state.userLoading = loading
    }
  },
  actions: {
    getUserInfo: function ({ commit }) {
      commit('setUserLoading', true)

      return http.fetchGet('/api/member/checkLogin').then(result => {
        commit('setUserLoading', false)

        if (result.code !== 200) {
          return result
        }

        commit('getUserInfo', result.result)
        return result
      }).catch(result => {
        commit('setUserLoading', false)
        return result
      })
    },
    login: function ({ commit }, params) {
      return http.fetchPost('/api/member/login', params)
    }
  }
}

export default app

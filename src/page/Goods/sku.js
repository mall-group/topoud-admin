/**
 * @param {object} product // 商品
 *   {
 *     skuEnable: 1,
 *     skuTags: [],
 *     sku: [],
 *     stockNum: 1,
 *   }
 *
 * @return // 所有 sku 标签
 *   {
 *     skuEnable: 1,
 *     skuTags: [{
 *       key: '颜色',
 *       value: [{
 *         value: '红色',
 *         isSelected: false,
 *         isDisabled: false
 *       }]
 *     }],
 *     price: {}, // 价格
 *     stockNum: 1, // 库存
 *     canBuy: false // 是否可以购买
 *   }
 */
export default function (product, selected) {
  // 默认值
  const ret = {
    skuEnable: 1,
    skuTags: [],
    price: {
      marketPrice: product.marketPrice || 0,
      salePrice: product.salePrice || 0,
      supplyPrice: product.supplyPrice || 0
    },
    stockNum: 0,
    canBuy: false
  }

  if (!product) {
    return ret
  }

  // 没有开启 sku
  if (!product.skuEnable) {
    ret.skuEnable = 0
    ret.price = {
      marketPrice: product.marketPrice || 0,
      salePrice: product.salePrice || 0,
      supplyPrice: product.supplyPrice || 0
    }
    ret.stockNum = product.stockNum
    ret.canBuy = ret.stockNum > 0
  }

  if (!product.skuTags || !product.skuTags.length) {
    return ret
  }
  if (!product.sku || !product.sku.length) {
    return ret
  }

  // 初始 sku 标签
  ret.skuTags = product.skuTags.map(tag => {
    return {
      ...tag,
      value: (tag.value || []).map(v => {
        return {
          value: v,
          isSelected: false,
          // 所有的 sku 组合都不包含该值，则不能选择
          isDisabled: product.sku.findIndex(s => s[tag.key] === v) === -1
        }
      })
    }
  })

  // 没有选中任何 sku，直接返回
  if (!selected || !selected.length) {
    return ret
  }

  // 所有 sku 项都选中了，可购买
  if (selected.length === product.skuTags.length) {
    const found = product.sku.find(s => {
      let ret = true
      selected.forEach(i => {
        if (s[i.key] !== i.value) {
          ret = false
        }
      })

      return ret
    })

    if (!found) {
      // 异常
    } else {
      ret.price = {
        marketPrice: found.marketPrice,
        salePrice: found.salePrice,
        supplyPrice: found.supplyPrice
      }
      ret.stockNum = found.stockNum
      ret.canBuy = ret.stockNum > 0
    }
  }

  // 遍历所有选中的项，设置 isSelected
  selected.forEach(s => {
    if (!s.key || !s.value) {
      return
    }

    // 多次选中同个标签，sku 标签只能单选
    const duplicated = ret.skuTags.find(i => {
      if (i.key !== s.key) {
        return false
      }

      return (i.value || []).find(v => v.isSelected)
    })
    if (duplicated) {
      return
    }

    const index = ret.skuTags.findIndex(i => i.key === s.key)
    if (index === -1) {
      return
    }

    ret.skuTags[index].value = (ret.skuTags[index].value || []).map(i => {
      // 每个 sku 都是单选
      if (i.value !== s.value) {
        i.isDisabled = true
        return i
      }

      i.isSelected = true
      return i
    })
  })

  // 把不在 sku 组合里的项给排除掉
  // 如选中了某颜色，则只有包含该颜色的尺寸，才可以被选择
  if (selected.length > 0) {
    let availableSku = [...product.sku]
    selected.forEach(s => {
      availableSku = availableSku.filter(a => a[s.key] === s.value)
    })

    ret.skuTags = ret.skuTags.map(tag => {
      tag.value = tag.value.map(i => {
        if (availableSku.find(a => a[tag.key] === i.value)) {
          return i
        }

        i.isDisabled = true
        return i
      })

      return tag
    })
  }

  return ret
}

export function selectSku (key, value, selected, skuTags) {
  // 当前项是否被已经 disabled
  var disabled = skuTags.find(t => {
    if (t.key !== key) {
      return false
    }

    const found = (t.value || []).find(v => v.value === value)
    return found && found.isDisabled
  })
  if (disabled) {
    return selected
  }

  if (!selected || !selected.length) {
    return [{
      key,
      value
    }]
  }

  // toggle
  if (selected.find(s => s.key === key && s.value === value)) {
    selected = selected.filter(s => !(s.key === key && s.value === value))
  } else {
    selected.push({
      key: key,
      value: value
    })
  }

  return selected
}

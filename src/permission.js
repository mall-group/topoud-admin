import router from './router'
import store from './store'
import WHITE_LIST from './utils/whiteList'

// eslint-disable-next-line
function hasPermission(roles, permissionRoles) {
  if (roles.indexOf('admin') >= 0) {
    return true // admin permission passed directly
  }

  if (!permissionRoles) {
    return true
  }

  return roles.some(role => permissionRoles.indexOf(role) >= 0)
}

// 用户信息是否加载完
router.beforeEach((to, from, next) => {
  // return next()
  if (!store.state.app.userLoading) {
    return next()
  }

  store.dispatch('getUserInfo').then(r => {
    setTimeout(next)
  }).catch(e => {
    setTimeout(next)
  })
})

router.beforeEach((to, from, next) => {
  // 用户未登录
  if (!store.state.app.isLogined) {
    // 白名单，直接进入
    if (WHITE_LIST.indexOf(to.path) !== -1) {
      return next()
    }

    if (to.path !== '/login') {
      next('/login')
    } else {
      next()
    }

    return
  }

  // 用户已登录
  if (to.path === '/login') {
    return next('/')
  }

  next()
})

import Vue from 'vue'
import VueLazyload from 'vue-lazyload'
import infiniteScroll from 'vue-infinite-scroll'
import VueCookie from 'vue-cookie'
import App from './App'
import router from './router'
import store from './store'
import './element-ui'
import './permission'
import './assets/iconfont/iconfont.css'

Vue.prototype.$http = require('axios').default

Vue.use(infiniteScroll)
Vue.use(VueCookie)
Vue.use(VueLazyload, {
  // preLoad: 1.3,
  // error: 'dist/error.png',
  // loading: '/static/images/load.gif'
  // attempt: 1
})
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})

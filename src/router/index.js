import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

// const Index = () => import('/page/index.vue')
const Login = () => import('/page/Login/login.vue')
const Register = () => import('/page/Login/register.vue')
const FindPwd = () => import('/page/Login/findpwd.vue')
const ResetPwd = () => import('/page/Login/resetpwd.vue')
// const Home = () => import('/page/Home/home.vue')
// const GoodS = () => import('/page/Goods/goods.vue')
// const goodsDetails = () => import('/page/Goods/goodsDetails.vue')
const Cart = () => import('/page/Cart/cart.vue')
const order = () => import('/page/Order/order.vue')
const user = () => import('/page/User/user.vue')
const orderList = () => import('/page/User/children/order.vue')
const information = () => import('/page/User/children/information.vue')
const addressList = () => import('/page/User/children/addressList.vue')
const coupon = () => import('/page/User/children/coupon.vue')
const aihuishou = () => import('/page/User/children/aihuishou.vue')
const support = () => import('/page/User/children/support.vue')
const checkout = () => import('/page/Checkout/checkout.vue')
const payment = () => import('/page/Order/payment.vue')
const paysuccess = () => import('/page/Order/paysuccess.vue')
// const Thanks = () => import('/page/Thanks/thanks.vue')
const Search = () => import('/page/Search/search.vue')
const RefreshSearch = () => import('/page/Refresh/refreshsearch.vue')
// const RefreshGoods = () => import('/page/Refresh/refreshgoods.vue')
const orderDetail = () => import('/page/User/children/orderDetail.vue')
const Alipay = () => import('/page/Order/alipay.vue')
const WechatPay = () => import('/page/Order/wechat.vue')
const QQpay = () => import('/page/Order/qqpay.vue')
const OpenStoreApply = () => import('/page/Store/apply.vue')
const Store = () => import('/page/Store/store.vue')
const StoreIndex = () => import('/page/Store/children/index.vue')
const StoreGoods = () => import('/page/GoodsManage/goods.vue')
const StoreGoodsDetail = () => import('/page/GoodsManage/goodsDetail.vue')
const StoreOrder = () => import('/page/Store/children/order.vue')
const StoreOrderDetail = () => import('/page/Store/children/orderDetail.vue')
const StorePanel = () => import('/page/Panel/panel.vue')
const StorePanelEdit = () => import('/page/Panel/panelEdit.vue')
const StorePanelContent = () => import('/page/Panel/panelContent.vue')
const StorePanelContentEdit = () => import('/page/Panel/panelContentEdit.vue')
const StoreCarousel = () => import('/page/Panel/carousel.vue')
const StoreCarouselEdit = () => import('/page/Panel/carouselEdit.vue')
const StoreActivityType = () => import('/page/Activity/activityType.vue')
const StoreActivity = () => import('/page/Activity/activity.vue')
const StoreActivityEdit = () => import('/page/Activity/activityEdit.vue')
const StoreActivityGroupBuyEdit = () => import('/page/Groupbuy/form.vue')
const StoreGroupBuyOrder = () => import('/page/Store/children/groupBuyOrder.vue')
const StoreGroupBuyOrderDetail = () => import('/page/Store/children/groupBuyOrderDetail.vue')
const Wechat = () => import('/page/Wechat/wechat.vue')
const WechatIndex = () => import('/page/Wechat/children/index.vue')
const WechatList = () => import('/page/Wechat/children/list.vue')
const WechatEdit = () => import('/page/Wechat/children/edit.vue')
const WechatMini = () => import('/page/Wechat/children/mini.vue')
const WechatMenu = () => import('/page/Wechat/children/menu.vue')
const WechatReply = () => import('/page/Wechat/children/reply/index.vue')
const WechatReplyEdit = () => import('/page/Wechat/children/reply/replyEdit.vue')
const WechatFans = () => import('/page/Wechat/children/fans.vue')
const WechatMaterial = () => import('/page/Wechat/children/material/index.vue')
const WechatMaterialBlog = () => import('/page/Wechat/children/material/blog.vue')
const WechatPayConfig = () => import('/page/Wechat/children/payConfig.vue')
const StoreBrand = () => import('/page/Store/children/brand.vue')
const StoreBrandDetail = () => import('/page/Store/children/brandDetail.vue')
const StoreCategory = () => import('/page/Category/category.vue')
const StoreCategoryAdd = () => import('/page/Category/categoryAdd.vue')
const SettingIndex = () => import('/page/Setting/index.vue')
const SettingStoreInfo = () => import('/page/Setting/storeInfo.vue')
const CommissionIndex = () => import('/page/Commission/index.vue')
const CommissionForm = () => import('/page/Commission/form.vue')
const ArticleIndex = () => import('/page/Article/index.vue')
const ThemeIndex = () => import('/page/Article/theme.vue')
const ArticleForm = () => import('/page/Article/form.vue')
const ArticleTypeIndex = () => import('/page/Article/typeIndex.vue')
const ArticleTypeForm = () => import('/page/Article/typeForm.vue')
const Back = () => import('/page/Store/children/back.vue')
const ChannelIndex = () => import('/page/Channel/index.vue')
const ChannelForm = () => import('/page/Channel/form.vue')
const ChannelMember = () => import('/page/Channel/member.vue')
const MemberForm = () => import('/page/Channel/memberForm.vue')
const ChannelReport = () => import('/page/Channel/report.vue')
const BargainForm = () => import('/page/Bargain/form.vue')
const MemberIndex = () => import('/page/Member/index.vue')
const Carriage = () => import('/page/Carriage/carriage.vue')
const CarriageAdd = () => import('/page/Carriage/add.vue')
const Showcase = () => import('/page/Showcase/showcase.vue')
const CouponTemplate = () => import('/page/Coupon/couponTemplate.vue')
const CouponCreate = () => import('/page/Coupon/couponCreate.vue')
const CouponGive = () => import('/page/Coupon/couponGive.vue')
const OperatorIndex = () => import('/page/Operator/index.vue')
const OperatorForm = () => import('/page/Operator/form.vue')
const NotifyIndex = () => import('/page/Notify/index.vue')
const PromotionIndex = () => import('/page/promotion/index.vue')
const TemplateIndex = () => import('/page/Template/storehome/index.vue')
const TemplateForm = () => import('/page/Template/storehome/form.vue')
const PosterStoreIndex = () => import('/page/Template/poster_store/index.vue')
const PosterStoreForm = () => import('/page/Template/poster_store/form.vue')
const PosterProductIndex = () => import('/page/Template/poster_product/index.vue')
const PosterProductForm = () => import('/page/Template/poster_product/form.vue')
const PosterCardIndex = () => import('/page/Template/poster_card/index.vue')
const PosterCardForm = () => import('/page/Template/poster_card/form.vue')
const WebsiteIndex = () => import('/page/Template/website/index.vue')
const WebsiteForm = () => import('/page/Template/website/form.vue')

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      // component: Index,
      component: Store,
      name: 'index',
      // redirect: '/home',
      children: [
        { path: '', component: StoreIndex }
        // { path: 'goods', component: GoodS },
        // { path: 'goodsDetails', name: 'goodsDetails', component: goodsDetails },
        // { path: 'thanks', name: 'thanks', component: Thanks },
        // { path: '/refreshgoods', name: 'refreshgoods', component: RefreshGoods }
      ]
    },
    { path: '/login', name: 'login', component: Login },
    { path: '/register', name: 'register', component: Register },
    { path: '/findpwd', name: 'findpwd', component: FindPwd },
    { path: '/resetpwd', name: 'resetpwd', component: ResetPwd },
    { path: '/cart', name: 'cart', component: Cart },
    { path: '/refreshsearch', name: 'refreshsearch', component: RefreshSearch },
    {
      path: '/order',
      name: 'order',
      component: order,
      children: [
        { path: 'paysuccess', name: 'paysuccess', component: paysuccess },
        { path: 'payment', name: 'payment', component: payment },
        { path: '/search', name: 'search', component: Search },
        { path: 'alipay', name: 'alipay', component: Alipay },
        { path: 'wechatPay', name: 'wechatPay', component: WechatPay },
        { path: 'qqpay', name: 'qqpay', component: QQpay }
      ]
    },
    {
      path: '/user',
      name: 'user',
      component: user,
      redirect: '/user/orderList',
      children: [
        { path: 'orderList', name: '订单列表', component: orderList },
        { path: 'orderDetail', name: '订单详情', component: orderDetail },
        // { path: 'information', name: '账户资料', component: information },
        { path: 'addressList', name: '收货地址', component: addressList },
        { path: 'coupon', name: '我的优惠', component: coupon },
        { path: 'support', name: '售后服务', component: support },
        { path: 'aihuishou', name: '以旧换新', component: aihuishou }
      ]
    },
    { path: '/checkout', name: 'checkout', component: checkout },
    { path: '*', redirect: '/home' },
    {
      path: '/openStoreApply',
      name: 'openStoreApply',
      component: OpenStoreApply,
      children: [
        { path: 'apply', name: 'apply', component: OpenStoreApply }
      ]
    },
    {
      path: '/information',
      name: 'information',
      component: information,
      children: [
        // { path: 'information', name: 'information', component: information }
      ]
    },
    {
      path: '/store',
      name: 'store',
      component: Store,
      redirect: '/store/storeIndex',
      children: [
        { path: 'storeIndex', name: 'storeIndex', component: StoreIndex },
        { path: 'storeGoods', name: 'storeGoods', component: StoreGoods },
        { path: 'storeGoodsDetail', name: 'storeGoodsDetail', component: StoreGoodsDetail },
        { path: 'storeOrder', name: 'storeOrder', component: StoreOrder },
        { path: 'storeOrderDetail', name: 'storeOrderDetail', component: StoreOrderDetail },
        { path: 'storePanel', name: 'storePanel', component: StorePanel },
        { path: 'storePanelEdit', name: 'storePanelEdit', component: StorePanelEdit },
        { path: 'storePanelContent', name: 'storePanelContent', component: StorePanelContent },
        { path: 'storePanelContentEdit', name: 'storePanelContentEdit', component: StorePanelContentEdit },
        { path: 'storeCarousel', name: 'storeCarousel', component: StoreCarousel },
        { path: 'storeCarouselEdit', name: 'storeCarouselEdit', component: StoreCarouselEdit },
        { path: 'storeActivityType', name: 'storeActivityType', component: StoreActivityType },
        { path: 'storeActivity', name: 'storeActivity', component: StoreActivity },
        { path: 'storeActivityEdit', name: 'storeActivityEdit', component: StoreActivityEdit },
        { path: 'storeActivityGroupBuyEdit', name: 'storeActivityGroupBuyEdit', component: StoreActivityGroupBuyEdit },
        { path: 'storeGroupBuyOrder', name: 'storeGroupBuyOrder', component: StoreGroupBuyOrder },
        { path: 'storeGroupBuyOrderDetail', name: 'storeGroupBuyOrderDetail', component: StoreGroupBuyOrderDetail },
        { path: 'wechatList', name: 'wechatList', component: WechatList },
        { path: 'wechatEdit', name: 'wechatEdit', component: WechatEdit },
        { path: 'wechatMini', name: 'wechatMini', component: WechatMini },
        { path: 'wechatPayConfig', name: 'wechatPayConfig', component: WechatPayConfig },
        { path: 'storeBrand', name: 'storeBrand', component: StoreBrand },
        { path: 'storeBrandDetail', name: 'storeBrandDetail', component: StoreBrandDetail },
        { path: 'storeCategory', name: 'storeCategory', component: StoreCategory },
        { path: 'storeCategoryAdd', name: 'storeCategoryAdd', component: StoreCategoryAdd },
        { path: 'settingIndex', name: 'settingIndex', component: SettingIndex },
        { path: 'settingStoreInfo', name: 'settingStoreInfo', component: SettingStoreInfo },
        { path: 'commissionIndex', name: 'commissionIndex', component: CommissionIndex },
        { path: 'commissionForm', name: 'commissionForm', component: CommissionForm },
        { path: 'articleIndex', name: 'articleIndex', component: ArticleIndex },
        { path: 'themeIndex', name: 'themeIndex', component: ThemeIndex },
        { path: 'articleForm', name: 'articleForm', component: ArticleForm },
        { path: 'articleTypeIndex', name: 'articleTypeIndex', component: ArticleTypeIndex },
        { path: 'articleTypeForm', name: 'articleTypeForm', component: ArticleTypeForm },
        { path: 'back', name: 'back', component: Back },
        { path: 'bargainForm', name: 'bargainForm', component: BargainForm },
        { path: 'memberIndex', name: 'memberIndex', component: MemberIndex },
        { path: 'carriage', name: 'carriage', component: Carriage },
        { path: 'carriageAdd', name: 'carriageAdd', component: CarriageAdd },
        { path: 'showcase', name: 'showcase', component: Showcase },
        { path: 'bargainForm', name: 'bargainForm', component: BargainForm },
        { path: 'channelIndex', name: 'channelIndex', component: ChannelIndex },
        { path: 'channelForm', name: 'channelForm', component: ChannelForm },
        { path: 'channelMember', name: 'channelMember', component: ChannelMember },
        { path: 'memberForm', name: 'memberForm', component: MemberForm },
        { path: 'channelReport', name: 'channelReport', component: ChannelReport },
        { path: 'coupon', name: 'coupon', component: CouponTemplate },
        { path: 'couponCreate', name: 'couponCreate', component: CouponCreate },
        { path: 'couponGive', name: 'couponGive', component: CouponGive },
        { path: 'operatorIndex', name: 'operatorIndex', component: OperatorIndex },
        { path: 'operatorForm', name: 'operatorForm', component: OperatorForm },
        { path: 'notifyIndex', name: 'notifyIndex', component: NotifyIndex },
        { path: 'posterStoreIndex', name: 'posterStoreIndex', component: PosterStoreIndex },
        { path: 'posterStoreForm', name: 'posterStoreForm', component: PosterStoreForm },
        { path: 'posterProductIndex', name: 'posterProductIndex', component: PosterProductIndex },
        { path: 'posterProductForm', name: 'posterProductForm', component: PosterProductForm },
        { path: 'posterCardIndex', name: 'posterCardIndex', component: PosterCardIndex },
        { path: 'posterCardForm', name: 'posterCardForm', component: PosterCardForm },
        { path: 'templateIndex', name: 'templateIndex', component: TemplateIndex },
        { path: 'templateForm', name: 'templateForm', component: TemplateForm },
        { path: 'websiteIndex', name: 'websiteIndex', component: WebsiteIndex },
        { path: 'websiteForm', name: 'websiteForm', component: WebsiteForm },
        { path: 'promotionIndex', name: 'promotionIndex', component: PromotionIndex }
      ]
    },
    {
      path: '/wechat/:wechatId',
      name: 'wechat',
      component: Wechat,
      redirect: '/wechat/wechatList',
      children: [
        { path: '', name: 'wechatIndex', component: WechatIndex },
        { path: 'reply', name: 'wechatReply', component: WechatReply },
        { path: 'replyEdit', name: 'wechatReplyEdit', component: WechatReplyEdit },
        { path: 'menu', name: 'wechatMenu', component: WechatMenu },
        { path: 'fans', name: 'wechatFans', component: WechatFans },
        { path: 'material', name: 'wechatMaterial', component: WechatMaterial },
        { path: 'blog', name: 'wechatMaterialBlog', component: WechatMaterialBlog },
        { path: 'wechatPayConfig', name: 'wechatPayConfig', component: WechatPayConfig }
      ]
    }
  ]
})
